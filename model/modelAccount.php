<?php

class modelAccount extends modelAbstract implements iModelAccount {
	var $myConnexion;
	
	/**
	* @var Singleton
	* @access private
	* @static
	*/
	private static $instance;

	public function __construct() {
	      parent::__construct();			
	}

	public function req() {
		$this->myConnexion = $this->connexion;
		return $this->myConnexion->query("SELECT * FROM account");
	}

	public function ajoutAccount($id, $email, $firstname, $lastname, $status, $addr1, $addr2, $city, $state, $zip, $country, $phone) {
		$this->myConnexion = $this->connexion;
		return $this->myConnexion->query("INSERT INTO account VALUES ('$id', '$email', '$firstname', '$lastname', '$status', '$addr1', '$addr2', '$city', '$state', '$zip', '$country', '$phone') ");
	}

	public function identificationCorrecte($login, $pass) {
		$this->myConnexion = $this->connexion;
		return $this->myConnexion->query("SELECT username FROM signon WHERE username = '".$login."' AND password = '".$pass."'");
	}
	
	/**
	* Méthode qui crée l'unique instance de la classe
	* si elle n'existe pas encore puis la retourne.
	*
	* @param void
	* @return Singleton
	*/
	public static function Instance() {
		static $inst = null;
		if ($inst === null) {
			$inst = new modelAccount();
			echo "<br/> --- DEBUG --- new instance";
		}
		return $inst;
	}

}

?>
