<?php
	class modelItem extends modelAbstract {
		var $myConnexion;
		
		function __construct() {
		      parent::__construct();
		}
		
		function recupListItem($prod) {	
			$this->myConnexion = $this->connexion;	
			return $this->myConnexion->query("SELECT * FROM item WHERE productid = '".$prod."'");			
		}

		// Retourne la balise image de la catégorie de l'item donné par sa réf
		public function getImageByItemId($ref) {
			$this->myConnexion = $this->connexion;			
			return $this->myConnexion->query("select descn from product where productid = (select productid from item where itemid = '".$ref."');");	
		}

		// Retourne le prix de l'item donné par sa réf
		public function getPriceByItemId($ref) {
			$this->myConnexion = $this->connexion;
			return $this->myConnexion->query("select listprice from item where itemid = '".$ref."';");	
		}
		
		public function getDispoByItemId($ref) {
			$this->myConnexion = $this->connexion;
			return $this->myConnexion->query("select count(*) AS 'estDispo' from inventory where itemid = '".$ref."' and qty >= 0;");	
		}
		
	}
?>
