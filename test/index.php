<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
		<title>Page de tests</title>
		<meta name="description" content="Testing page">
		<meta name="author" content="Souris Mécanique">
		<link rel="stylesheet" href="css/reset.css?v=1.0">
		<link rel="stylesheet" href="css/main.css?v=1.0">
		<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
<body>
	<script src="../js/scripts.js"></script>
	<?php 
	
		echo "-- Chargement du model<br/>";
		include('../model/model.php');

		echo "-- Chargement du test de modelAccount<br/>";
		include('./modelAccount.php');

		echo "-- Chargement du test de modelItem<br/>";
		include('./modelItem.php');
	?>
</body>
</html>
