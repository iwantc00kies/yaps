<?php
	$model = new modelItem();


	/** Utilisation de la méthode getImageByItemId **/
	echo"<li><b>Test de getImageByItemId</b></li>";
	echo "<p>Retourne la balise image de la catégorie de l'item donné par sa réf</p>";

	$objects = $model->getImageByItemId("EST-1");
	$objects->setFetchMode(PDO::FETCH_OBJ); 
	$enregistrement = $objects->fetch();

	echo $enregistrement->descn;


	/** Utilisation de la méthode getPriceByItemId **/
	echo"<li><b>Test de getPriceByItemId</b></li>";
	echo "<p>Retourne le prix de l'item donné par sa réf</p>";

	$objects = $model->getPriceByItemId("EST-1");
	$objects->setFetchMode(PDO::FETCH_OBJ); 
	$enregistrement = $objects->fetch();

	echo $enregistrement->listprice;
?>
