<?php
	$model = modelAccount::Instance();


	/** Utilisation de la méthode req **/
	echo"<li><b>Test d'affichage d'un account</b></li>";

	$objects = $model->req();
	$objects->setFetchMode(PDO::FETCH_OBJ); // Nécessaire pour mettre les résultats sous forme "objets"

	while( $enregistrement = $objects->fetch() )
	{
		echo '<p>', $enregistrement->userid, ' ', $enregistrement->email, '</p>';
	}


	/** Utilisation de la méthode identificationCorrecte **/
	echo"<li><b>Test d'une identification erronee</b></li>";

	$objects = $model->identificationCorrecte("ds", "d");
	$objects->setFetchMode(PDO::FETCH_OBJ);
	$enregistrement = $objects->fetch();

	if ( empty($enregistrement) ) {
		echo"<p>Erreur d'identification</p>";
	}
	else {
		echo"<p>Identification OK</p>";
	}


	/** Utilisation de la méthode identificationCorrecte **/	
	echo"<li><b>Test d'une identification correcte</b></li>";

	$objects = $model->identificationCorrecte("j2ee", "j2ee");
	$objects->setFetchMode(PDO::FETCH_OBJ);
	$enregistrement = $objects->fetch();

	if ( empty($enregistrement) ) {
		echo"<p>Erreur d'identification</p>";
	}
	else {
		echo"<p>Identification OK</p>";
	}


	/** Utilisation de la méthode ajoutAccount **/	
	echo"<li><b>Test d'ajout d'un utilisateur</b></li>";

	$date = date_create();
	$dateTimeStamp = date_timestamp_get($date);

	$objects = $model->ajoutAccount("j2ee".$dateTimeStamp, "yourname@yourdomain.com", "ABC", "XYX", "OK", "901 San Antonio Road", "MS UCUP02-206", "Palo Alto", "CA", "94303", "USA", "555-555-5555");

	if (!empty($objects)) {
		echo "<p>Enregistrement de j2ee".$dateTimeStamp." réussi</p>";
	}
	else {
		echo "<p>Enregistrement de j2ee".$dateTimeStamp." échoué</p>";
	}


	/** Utilisation de la méthode ajoutAccount **/
	echo"<li><b>Test d'ajout d'un utilisateur avec une id déjà rentrée</b></li>";

	$objects = $model->ajoutAccount("j2ee".$dateTimeStamp, "yourname@yourdomain.com", "ABC", "XYX", "OK", "901 San Antonio Road", "MS UCUP02-206", "Palo Alto", "CA", "94303", "USA", "555-555-5555");

	if (!empty($objects)) {
		echo "<p>Enregistrement de j2ee".$dateTimeStamp." réussi</p>";
	}
	else {
		echo "<p>Enregistrement échouéde j2ee".$dateTimeStamp."</p>";
	}


?>
