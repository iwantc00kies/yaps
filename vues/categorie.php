<?php
	// TODO par le controler
	$isConnected = True;

	$cat = $_GET["cat"];

	$titre = "YAPS - Affichage ".$cat;
	$necessiteModel = true;
	$necessiteControleur = false; // TODO: True
	include("./elements/entete.php");

	$modelP = new modelProduct();
	$modelI = new modelItem();

	// Récupération de la liste des catégories
	$listeCat = $modelP->RecupListcat();
	$listeCat->setFetchMode(PDO::FETCH_OBJ);

	// Récupération des items d'une catégorie
	$listeProduct = $modelP->RecupListProd($cat);
	$listeProduct->setFetchMode(PDO::FETCH_OBJ);
	
?>
	<h1>Voici les produits en rapport avec la catégorie <?php echo $cat; ?></h1>
		<div class="textRech">
		</div>
		<h2>Différents types de produit pour cette catégorie</h2>
		<table>
			<tr>
				<th>Type de produit</th>
				<th>Produits</th>
			</tr>
<?php		
			// Pour chaque type de produit de la catégorie correspondante
			while( $product = $listeProduct->fetch() ) {								
				// On récupère chaque produit associé
				$listeItem = $modelI->recupListItem($product->productid);
				$listeItem->setFetchMode(PDO::FETCH_OBJ);
				
				echo 	'<tr>
							<td>'.$product->descn.'</td>
							<td>
								<table>
									<tr>
										<th>Attr</th>
										<th>Dispo</th>
										<th>prix</th>
										<th>Action</th>
									</tr>
						';
						
				
				while( $item = $listeItem->fetch() ) {				
					echo 			'<tr>';
					echo 				'<td>'.$item->attr1.'</td>';	
					echo 				'<td>'.$item->status.'</td>';
					echo 				'<td>'.$item->listprice.'</td>';									
					if ($isConnected) {
						echo 			'<td><a href="#">Ajouter au panier</a></td>';
					}
					else {
						echo 			'<td></td>';
					}
					echo 			'</tr>';
				}
				echo 			'</table>
							</td>
						</tr>';
			}
?>
		</table>
		<h2>Changer de catégorie</h2>
		<form action="categorie.php" method="GET">
			<select name="cat">
				<option  value="">Toutes les catégories</option>		
				<?php		
				while( $categorie = $listeCat->fetch() ) {
					// dans value : id de la catégorie
					echo '<option  NAME="cat" value="'.$categorie->name.'">'.$categorie->name.'</option>'; 
				}
				?>
			</select>
			<input type="submit"/>
		</form>
<?php
	include ('./elements/pied_de_page.php');
?>		

	   



