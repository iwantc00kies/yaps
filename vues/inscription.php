<?php
	$titre = "YAPS - Accueil";
	$necessiteModel = true;
	$necessiteControleur = false;
	include("./elements/entete.php");

	// Récupération de la liste des catégories
	$model = new modelProduct();
	$objects = $model->RecupListcat();
	$objects->setFetchMode(PDO::FETCH_OBJ);
?>

	
	<h1>Projet : Yet Another Pet Store </h1>

	<form id="inscription" method="post" action="inscription.php" >
		<fieldset>
			<legend>Formulaire d'inscription</legend>
		</fieldset>	
	</form>
	
	<form id="connexion" method=post>
		<fieldset>
			<legend>Déjà connecté ?</legend>
			<label>Identifiant</label>
			<input type="text" name="username" /> <br />
			<label>Mot de passe</label>
			<input type="password" name="password" />
			
			<input type="submit" value="Connecter">
		</fieldset>
	</form>
  
<?php
	include ('./elements/pied_de_page.php');
?>		
