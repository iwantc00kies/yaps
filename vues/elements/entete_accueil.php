<!DOCTYPE html>
<html lang="fr">

<!-- initialisation habituelle d'une page xhtml 5-->
	<head>
		<title>YAPS</title><!-- declaration du titre la page -->
		<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8"/> <!-- précision de l'encodage du type de page  -->
		<link rel="stylesheet" href="../css/style.css" type="text/css"/> 	<!-- mise en place du lien avec le fichier css -->
		<link rel="stylesheet" href="../css/mm_training.css" type="text/css"/> 
		<script type="text/javascript" src="../js/jquery-1.7.2.js"></script>
		<script type="text/javascript" src="../js/connexion_accueil.js"></script>
		<script type="text/javascript" src="../js/jquery.maphilight.min.js"></script> 
		<script type="text/javascript" src="../js/jquery.tooltip.min.js" type="text/javascript"></script>
        <script src="js/we_scripts.js" type="text/javascript"></script>
	</head>
