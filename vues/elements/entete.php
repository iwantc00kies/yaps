<?php
	if ($necessiteModel) {
		require_once('../model/model.php'); 
	}
	if ($necessiteControleur) {
//		require_once('../controler/foo.php'); 
	}
?>

<!DOCTYPE html>
<html lang="fr">

	<!-- initialisation habituelle d'une page html 5-->
	<head>
		<title><?php echo $titre; ?></title>
		
		<!-- Les informations relatives au site -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="description" content="Site d'une animalerie">
		<meta name="auteur" content="Les Souris mécaniques">
		
		<!-- Déclarations des feuilles de style -->
		<link rel="stylesheet" href="../css/reset.css" type="text/css"/> 	
		<!-- <link rel="stylesheet" href="../css/style.css" type="text/css"/> -->
		<link rel="stylesheet" href="../css/main.css" type="text/css"/>

		<!-- Déclarations des scripts -->
		<script type="text/javascript" src="../js/connexion_accueil.js"></script>
		<script type="text/javascript" src="../js/jquery.maphilight.min.js"></script> 
		<script type="text/javascript" src="../js/jquery.tooltip.min.js" type="text/javascript"></script>
	</head>

	<body>
		<header id="header"></header>
		<span id="loginBox"></span>
		<?php include ('./elements/menu.php'); ?>
		<section id="page">
		<!--- Début de la section -->
