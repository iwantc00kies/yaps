<?php
	$titre = "YAPS - Accueil";
	$necessiteModel = true;
	$necessiteControleur = false;
	include("./elements/entete.php");

	// Récupération de la liste des catégories
	$model = new modelProduct();
	$objects = $model->RecupListcat();
	$objects->setFetchMode(PDO::FETCH_OBJ);
?>

	<img id="splash" src="../images/splash.png" />
	
	<h1>Bienvenue sur Yet Another Pet Store !</h1>
	<h2>Différentes catégories de l'animalerie</h2>

	<ul id="cat">			
	<?php 			
		while( $enregistrement = $objects->fetch() ) {									
			echo "<li><a href=categorie.php?cat=$enregistrement->catid >$enregistrement->descn</image></a></li>\n";
		}						
	?>
	</ul>

<?php
	include ('./elements/pied_de_page.php');
?>
