<?php

/* Déclarations des interfaces associées aux modèles */
/**
* @var Singleton
* @access private
* @static
*/
interface iModelAccount {

	/**
	* @var Singleton
	* @access private
	* @static
	*/
	public function req();

	// Ajouter un compte
	// Retourne la requête sous la forme : "PDOStatement Object ( [queryString] => INSERT INTO account VALUES ('j2ee1363796363', 'yourname@yourdomain.com', 'ABC', 'XYX', 'OK', '901 San Antonio Road', 'MS UCUP02-206', 'Palo Alto', 'CA', '94303', 'USA', '555-555-5555') )"
	// Postcondition : le retour est vide si la requête a échouée (possiblement un identifiant déjà existant dans la base) ; le test peut se faire avec " empty($foo) "			
	public function ajoutAccount($id, $email, $firstname, $lastname, $status, $addr1, $addr2, $city, $state, $zip, $country, $phone);

	// Vérifier correspondance identifiant - mot de passe
	public function identificationCorrecte($login, $pass)	;
}

interface iModelItem {

	// Retourne la balise image de la catégorie de l'item donné par sa réf
	public function getImageByItemId($ref);

	// Retourne le prix de l'item donné par sa réf
	public function getPriceByItemId($ref);	

}

?>
