<?php



class ControlAbstract {
	var $connexion;
	
	public function __construct() {
		require_once('../conf/connexion.php');			
		include('../conf/connexion.php');
	}		

}




/**
 * Auto-chargement des classes du controlleur
 * (ex. /controller/Test.class.php)
 */
function chargerClasse($classe)
{
	require_once  realpath($_SERVER["DOCUMENT_ROOT"]) .'/controller/' . ucfirst($classe) . '.class.php';

	//$classPath = realpath($_SERVER["DOCUMENT_ROOT"]) .'/controller/' . $classe . '.class.php' ;
	//	if(!class_exists('AClass')){
	//		require_once  $classPath;
	//	}


	//echo realpath($_SERVER["DOCUMENT_ROOT"]) .'/controler/' . $classe . '.class.php </br>';
	//require_once  realpath($_SERVER["DOCUMENT_ROOT"]) .'/controller/' . $classe . '.class.php';
	//require_once $root . $classe . '.class.php';
	//var_dump( spl_autoload_functions( ) );
}


/**
 * Classe controlleur principale de l'application
 */
class Controller extends ControlAbstract {


	/**
	 * constructeur du controlleur
	 */
	public function __construct()
	{
 		parent::__construct();	
		echo "<br/> nouveau controlleur <br/>";	
		// auto-chargement
		spl_autoload_extensions(".php");
		if(!spl_autoload_functions()) {
			spl_autoload_register('chargerClasse');
		}
	}

	/**
	 * Hydratation des données
	 */
	public function hydrate(array $donnees)
	{
		foreach ($donnees as $key => $value)
		{
			$method = 'set'.ucfirst($key);
			if (method_exists($this, $method))
			{
				$this->$method($value);
			}
		}
	}





	/**
	 * Méthode magique appelee lorsqu'un set inexistant est appelé
	 * @param unknown_type $nom
	 * @param unknown_type $valeur
	 */
	//	public function __set($nom, $valeur)
	//	{
	//		echo 'Ah, on a tenté d\'assigner à l\'attribut <strong>', $nom, '</strong> la valeur <strong>', $valeur, '</strong> mais c\'est pas possible !<br />';
	//	}


	//	public function __call($nom, $arguments)
	//	{
	//		echo 'La méthode <strong>', $nom, '</strong> a été appelée alors qu\'elle n\'existe pas ! Ses arguments étaient les suivants : <strong>', implode($arguments, '</strong>, <strong>'), '</strong>';
	//	}



	/**
	 * returns the root path of the project
	 */
	public static function root()
	{
		return realpath($_SERVER["DOCUMENT_ROOT"]);
	}



	/*******************
	 * Methodes metier
	 * *****************/


	/*******************
	 * LOGIN
	 * *****************/

	/**
	 *
	 * @return true si l'utilisateur est loggé, false sinon
	 */
	public static function isRegistered()
	{
		if (isset($_SESSION['myusername'])) {
			$registeredUser=true;
		}
		else {
			$registeredUser=false;
		}
		return $registeredUser;
	}



	/**
	 * Renvoie le contenu du panier 
	 * (Tableau associatif Reference => Quantite )
	 */
	function contenuPanier() {
 		return $_SESSION['cart'] ;
	}


	/*******************
	 * GESTION DU PANIER
	 * Fonctions d'affichage
	 * *****************/



	/**
	 * @return		returns the total number of items in the shopping cart
	 */
	function itemNumber() {

		$cart = $_SESSION['cart'];
		if (!$cart) {
			return 0;
		}
		else {
			$contents = array();
			$count = 0;
			// count item number
			foreach ($cart as $key => $value) {
				$count += intval($value)  ;
			}
		 return $count ;
		}
	}


	/**
	 * @return		item or items or nothing depending on the number of item(s)
	 */
	function itemOrItems() {

		$s =(itemNumber() > 1) ? 's':'';
		return $count.' item'.$s ;

	}


}

?>
