<?php

	//require('interface/interfacesModel.php');

	class ControlAbstract {
		var $connexion;
		
		public function __construct() {
			require_once('../conf/connexion.php');			
			include('../conf/connexion.php');
		}		
		
	}

	class Controleur extends ControlAbstract {
		var $myConnexion;
		
		/**
		* @var Singleton
		* @access private
		* @static
		*/
		private static $instance;

		public function __construct() {
		      parent::__construct();			
		}
		
		/*******************
	 * Methodes metier
	 * *****************/


	/*******************
	 * LOGIN
	 * *****************/

	/**
	 *
	 * @return true si l'utilisateur est logg�, false sinon
	 */
	public function isRegistered()
	{
		if (isset($_SESSION['name'])) {
			$registeredUser=true;
		}
		else {
			$registeredUser=false;
		}
		return $registeredUser;
	}

/**
 * update $_SESSION['cart']
 * ajoute ou retire un objet dans le panier
 */
public function update_cart() {

	$action = $_POST['action'] ;
	$id = $_POST['id'] ;

	// protect from injection
	$action = stripslashes($action);
	$id = stripslashes($id);
	$action = mysql_real_escape_string($action);
	$id = mysql_real_escape_string($id);


	// execute action
	$cart = $_SESSION['cart'];

	switch ($action) {
		case 'add' :
			// TODO verifier que la reference existe et est disponible
			$cart[$_POST['id']] += 1 ;
			break ;

		case 'del' :
			// TODO tester
			if(isset($cart[$_POST['id']]) && is_int($cart[$_POST['id']]) && $cart[$_POST['id']] > 0) {
				$cart[$_POST['id']] -= 1 ;
			}
			// TODO sinon erreur ?
			break ;

		default :
			echo 'invalid command </br>' ;
			break;
	}
$_SESSION['cart'] = $cart;
	
}






		
		/**
		* M�thode qui cr�e l'unique instance de la classe
		* si elle n'existe pas encore puis la retourne.
		*
		* @param void
		* @return Singleton
		*/
		public static function Instance() {
			static $inst = null;
			if ($inst === null) {
				$inst = new Controleur();
				echo "<br/> --- DEBUG --- new instance";
			}
			return $inst;
		}

	}
?>